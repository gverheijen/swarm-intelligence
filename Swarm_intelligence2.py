import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import time
import sys
from sklearn.neighbors import KDTree
from tqdm import tqdm
import matplotlib
matplotlib.rcParams['animation.ffmpeg_path'] = r"C:\\Users\\GidoVerheijen\\Documents\\Studie\\Natural Computing\\ffmpeg-2023-03-20-git-57afccc0ef-essentials_build\\bin\\ffmpeg.exe"

def formula(x, y, xs, ys):
    theta = np.arctan(np.abs((x-dimensions[0]/2)/(y-dimensions[1]/2)))
    #print(theta / np.pi)
   # print(theta)
    x_1 = np.cos(theta)**(2/8) * xs
    y_1 = np.sin(theta)**(2/8) * ys
    if x < dimensions[0]/2:
        x_1 = -1*x_1
    if y < dimensions[1]/2:
        y_1 = -1*y_1
    x_1 = x_1 + dimensions[0]/2
    y_1 = y_1 +  dimensions[1]/2
    return np.array([x_1, y_1])

def formula2(theta, xs, ys):
    x = np.random.rand() * (xs[1]-xs[0]) + xs[0]
    y = np.random.rand() * (ys[1]-ys[0]) + ys[0]
    x_1 = np.cos(theta)**(2/4) * x
    y_1 = np.sin(theta)**(2/4) * y
    if np.random.rand() < 0.5:
        x_1 = -1*x_1
    if np.random.rand() < 0.5:
        y_1 = -1*y_1
    x_1 = x_1 + dimensions[0]/2
    y_1 = y_1 +  dimensions[1]/2
    return np.array([x_1, y_1])



def push_back(pos):
    x, y = pos
    theta = np.arctan2((y-dimensions[1]/2),(x-dimensions[0]/2))

    m1 = ((x-dimensions[0]/2)/2.5)**8 + ((y-dimensions[1]/2)/1.45)**8
    m2 = ((x-dimensions[0]/2)/3.7)**8 + ((y-dimensions[1]/2)/2.25)**8
    if  m1 < 1:
        return   np.array([np.cos(theta), np.sin(theta)])
    elif m2 > 1:
        return -1  * np.array([np.cos(theta), np.sin(theta)])
    else:
        return np.array([0,0])

def circle_force(pos, x,y):

        if pos[0] < 7.5 and pos[0] > 2.5:
            return np.array([np.sign(pos[1]-3), 0])
        elif pos[1] < 4.5 and pos[1] > 1.5:
            return np.array([0, -1*np.sign(pos[0] - 5)])
        else:
            return np.array([np.sign(pos[1]-3), -1*np.sign(pos[0]-5)])

class particle():
    def __init__(self, xlim, ylim):
        #self.pos = np.random.rand(2)*np.array([xlim, ylim])
        self.angle_value = (np.random.rand() * 2 - 1) * np.pi
        angle = np.random.rand() * 0.5 * np.pi
        self.pos = formula2(angle, [2.5, 3.7], [1.45, 2.25])
        #print(self.pos)
        #self.vel = np.random.rand(2)*2 - 1
        self.xlim = xlim
        self.ylim = ylim
        self.vel = np.array([0,0])#np.array([np.cos(self.angle_value), np.sin(self.angle_value)])


    def angle(self):
        return self.angle_value

    def step(self, time_step, vel):

        r = circle_force(self.pos, self.xlim, self.ylim)

       # print(theta/np.pi)

        p = push_back(self.pos)
        if np.sum(p) != 0:
            self.vel = vel + 2*p
            self.vel = 2* self.vel / np.sqrt(np.sum(self.vel**2))
        else:
            self.vel = vel + r
            self.vel = self.vel / np.sqrt(np.sum(self.vel**2))
       # self.vel = r

        #self.vel = vel
        pot_pos = self.pos + self.vel * time_step
        self.pos = pot_pos
        #self.loop_back()
        self.angle_value = np.arctan2(vel[1], vel[0])

    def loop_back(self):
        #r = np.random.rand()/1000
        if self.pos[0] > self.xlim:
            self.pos[0] -= self.xlim
        if self.pos[0] < 0:
            self.pos[0] += self.xlim
        if self.pos[1] > self.ylim:
            self.pos[1] -= self.ylim
        if self.pos[1] < 0:
            self.pos[1] += self.ylim

class particle_group():
    def __init__(self, d, number):
        self.particle_array = np.array([particle(d[0], d[1]) for i in range(number)])
        print(self.pos())
        self.xlim = d[0]
        self.ylim = d[1]

    def pos(self):
        return np.array([a.pos for a in self.particle_array])

    def angles(self):
        return np.array([a.angle() for a in self.particle_array])

    def vels(self):
        return np.array([a.vel for a in self.particle_array])

    def step(self, time_step, parameters):
        distance, boid_size, alignment, cohesion, separation, cohesion_div, separation_div = parameters
        self.get_points_within_distance(boid_size)
        #print(self.neighbors)
        neigh = self.neighbors
        self.get_points_within_distance(distance)
        for n, a in enumerate(self.particle_array):
            base = a.angle_value
            avg_angle = base
            vel_coh = np.array([0,0])
            vel_alig = np.array([0,0])
            vel_sep = np.array([0,0])
            if alignment:
                avg_angle = self.get_average_angle(n, distance)
                if avg_angle != False:
                    vel_alig = np.array([np.cos(avg_angle), np.sin(avg_angle)])*0.05
            if cohesion:
                avg_pos = self.periodic_avg(n)
                if len(avg_pos)>1:
                    coor = avg_pos - a.pos
                    vel_coh = coor * cohesion_div
                   # print(avg_angle)
            if separation:
                force = self.distance(n, neigh)
                if len(force) > 1:
                    vel_sep = force * separation_div
            if separation or alignment or cohesion:
                vel =  vel_alig + vel_coh + vel_sep
            else:
                vel = a.vel
           # print(vel)
            a.step(time_step, vel)


    def get_points_within_distance(self, d):
        arr = self.pos()
        n = arr.shape[0]
        indices = [[] for _ in range(n)]

        # build the KD-Tree
        tree = KDTree(arr)

        # query the tree for neighbors within a distance d
        for i in range(n):
            neighbors = tree.query_radius([arr[i]], r=d)[0]
            neighbors = list(set(neighbors))
            for neighbor in neighbors:
                if neighbor != i:
                    indices[i].append(neighbor)

        self.neighbors = indices

    def periodic_avg(self, id):
        n = self.neighbors[id]
        points = self.pos()[n]
        if len(points) > 0:
           return np.average(points, axis = 0)
        else:
            return [False]

    def distance(self, id, n):
        n = n[id]
        points = self.pos()[n]
        if len(points) > 0:
           delta = -1* (points - self.pos()[id])
           distances = (delta[:,0]**2 + delta[:,1]**2)
           force = []
           for i,j in zip(distances, delta):
               force.append((1/i)*j)
           return np.average(force, axis = 0)
        else:
            return [False]
    """
    def get_average_pos(self, id):
        n = self.neighbors[id]
        #print(n)
        positions = self.pos()[n]
        avg = 0
        if len(positions) > 0:
            #for p in positions:

        else:
            return [False]
"""
    def get_average_angle(self, id, distance):
        #n = self.get_neighbours(id, distance)
        n = self.neighbors[id]
        angles = self.angles()[n]
        if len(angles) > 0:
            sins = np.average(np.sin(angles))
            coss = np.average(np.cos(angles))
            avg = np.arctan2(sins, coss)
            #print(avg)
            return avg
        else:
            return False


    def __call__(self, time_step, parameters):
        self.step(time_step, parameters)
        #time.sleep(0.001)
        return self.pos()





def run_simulation(pre_load, dimensions, number_of_particles, time_step, distance, boid_size, alignment, cohesion, separation, cohesion_div = 30, separation_div = 30):
    def frames():
        while True:
            if plt.fignum_exists(fig_nr):
                yield particles(time_step, [distance, boid_size, alignment, cohesion, separation, cohesion_div, separation_div])
            else:
                return np.array([[0,0]])

    def animate(args):
        #print(args)
        pos = args
        ax.clear()
        ax.set_xlim([0,dimensions[0]])
        ax.set_ylim([0, dimensions[1]])
        theta = np.linspace(0, np.pi/2, 1000)

        #print(sin)
        n = 8
        x1_1 = np.cos(theta)**(2/n) * 2.5 + dimensions[0]/2
        x1_2 = -1*np.cos(theta)**(2/n)*2.5 + dimensions[0]/2
        y1_1 = -1* np.sin(theta)**(2/n) * 1.45 + dimensions[1]/2
        y1_2 = np.sin(theta)**(2/n) * 1.45 + dimensions[1]/2
        plt.plot(x1_1, y1_1, c = 'black')
        plt.plot(x1_1, y1_2, c= 'black')
        plt.plot(x1_2, y1_1, c = 'black')
        plt.plot(x1_2, y1_2, c = 'black')

        x2_1 = np.cos(theta)**(2/n) * 3.7 + dimensions[0]/2
        x2_2 = -1*np.cos(theta)**(2/n) * 3.7+ dimensions[0]/2
        y2_1 = np.sin(theta)**(2/n) * 2.25 + dimensions[1]/2
        y2_2 = -1*np.sin(theta)**(2/n) * 2.25 + dimensions[1]/2
        plt.plot(x2_1, y2_1, c = 'black')
        plt.plot(x2_1, y2_2, c= 'black')
        plt.plot(x2_2, y2_1, c = 'black')
        plt.plot(x2_2, y2_2, c = 'black')
        #plt.plot(x,y1)
       # plt.plot(x2,y2)
        #plt.quiver(pos[:,0], pos[:,1], vels[:,0], vels[:,1])
        #plt.scatter(np.average(pos[:,0]), np.average(pos[:,1]), c = 'red')
        #plt.quiver(np.average(pos[:,0]), np.average(pos[:,1]), np.average(vels[:,0]), np.average(vels[:,1]))
        plt.scatter(pos[0, 0], pos[0, 1], c = 'red')
        return plt.scatter(pos[1:, 0], pos[1:, 1], c = 'blue')

    anim_running = True

    def onClick(event):
        nonlocal anim_running
        if anim_running:
            anim.event_source.stop()
            anim_running = False
        else:
            anim.event_source.start()
            anim_running = True



    particles = particle_group(dimensions, number_of_particles)
    #return particles.get_nearby_angles(particles.pos()[0], 0.1)
    #print(particles.angles())
    fig, ax  = plt.subplots(1,1)
    fig.canvas.mpl_connect('button_press_event', onClick)
    fig_nr = fig.number
    print(fig_nr)
    ax.set_xlim([0,dimensions[0]])
    ax.set_ylim([0, dimensions[1]])
    f = r"C://Users/GidoVerheijen/Documents/Studie/Natural Computing/"
    if pre_load:
        frame = []
        for i in tqdm(range(1000)):
            frame.append(list(next(frames())))
        frame = np.array(frame)
        anim = animation.FuncAnimation(fig, animate, frames = frame, interval = 15)
        np.save(f + f"n{number_of_particles}a{int(alignment)}c{int(cohesion)}s{int(separation)}t{time_step}v{distance}b{boid_size}cd{cohesion_div}sd{separation_div}.out", frame)

    else:
        anim = animation.FuncAnimation(fig, animate, frames = frames, interval = 15)
    f = r"C://Users/GidoVerheijen/Documents/Studie/Natural Computing/"
    writervideo = animation.FFMpegWriter(fps = 60)
    anim.save(f + f"n{number_of_particles}a{int(alignment)}c{int(cohesion)}s{int(separation)}t{time_step}v{distance}b{boid_size}cd{cohesion_div}sd{separation_div}.mp4", writer = writervideo)
    #plt.show()
    #print(particles.angles())
    #sys.exit(0)
    #return particle_array



    return particles

def vector_field():
    size = 15
    xx, yy = np.meshgrid(np.linspace(0,10, size), np.linspace(0,6, size))
    l = []
    vectors = []
    for i in range(size):
        for j in range(size):
            l.append([xx[j,i], yy[j,i]])
            v1 = circle_force([xx[j,i], yy[j,i]], 10, 6)

            v2 = push_back([xx[j,i], yy[j,i]])
            if np.sum(v2) == 0:
                v = v1
            else:
                v =v2
            s = np.sqrt(np.sum(v**2))
            if s != 0:
                vectors.append(v /s )
            else:
                vectors.append(v)
    l = np.array(l)
    vectors = np.array(vectors)

    fig, ax = plt.subplots(1,1)
    ax.set_xlim([0,dimensions[0]])
    ax.set_ylim([0, dimensions[1]])
    theta = np.linspace(0, np.pi/2, 1000)

    #print(sin)
    n = 8
    x1_1 = np.cos(theta)**(2/n) * 2.5 + dimensions[0]/2
    x1_2 = -1*np.cos(theta)**(2/n)*2.5 + dimensions[0]/2
    y1_1 = -1* np.sin(theta)**(2/n) * 1.45 + dimensions[1]/2
    y1_2 = np.sin(theta)**(2/n) * 1.45 + dimensions[1]/2
    plt.plot(x1_1, y1_1, c = 'black')
    plt.plot(x1_1, y1_2, c= 'black')
    plt.plot(x1_2, y1_1, c = 'black')
    plt.plot(x1_2, y1_2, c = 'black')

    x2_1 = np.cos(theta)**(2/n) * 3.7 + dimensions[0]/2
    x2_2 = -1*np.cos(theta)**(2/n) * 3.7+ dimensions[0]/2
    y2_1 = np.sin(theta)**(2/n) * 2.25 + dimensions[1]/2
    y2_2 = -1*np.sin(theta)**(2/n) * 2.25 + dimensions[1]/2
    plt.plot(x2_1, y2_1, c = 'black')
    plt.plot(x2_1, y2_2, c= 'black')
    plt.plot(x2_2, y2_1, c = 'black')
    plt.plot(x2_2, y2_2, c = 'black')
    plt.quiver(l[:,0], l[:,1], vectors[:,0], vectors[:,1])
    plt.show()






if __name__ == '__main__':
    pre_load = True
    dimensions = [10,6]
    number_of_particles = [1, 10, 20, 50, 100, 200, 500, 1000]
    time_step = 0.07
    particle_vision = 0.5
    boid_size = 0.1
    alignment = True
    cohesion = True
    separation = True
    cohesion_p = 1
    separation_p = 1
   # vector_field()


    for i in number_of_particles:
        a = run_simulation(pre_load, dimensions, i, time_step, particle_vision, boid_size, alignment, cohesion, separation, cohesion_p, separation_p)
    #print(a)